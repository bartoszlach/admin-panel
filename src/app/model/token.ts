export interface IToken {
  id: number;
  name: string;
  code: string;
  points: number;
  isMultiple: boolean;
}
