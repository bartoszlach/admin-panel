import { IToken } from './token';
export interface IUserToken extends IToken {
  date?: string;
}
