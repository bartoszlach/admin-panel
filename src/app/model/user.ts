export interface IUser {
  id: number;
  nick: string;
  points: number;
}
