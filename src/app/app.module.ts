import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

export class AppRoutingModule {}


import { AppComponent } from './app.component';
import { MaterialModule } from './material.module';
import { UserListComponent } from './user-list/user-list.component';
import { UserDetailComponent } from './user-detail/user-detail.component';
import { TokenListComponent } from './token-list/token-list.component';
import { UserTokenListComponent } from './user-token-list/user-token-list.component';
import { HeaderComponent } from './navigation/header/header.component';

@NgModule({
  declarations: [
    AppComponent,
    UserListComponent,
    UserDetailComponent,
    TokenListComponent,
    UserTokenListComponent,
    HeaderComponent,

  ],
  imports: [
    FormsModule,
    BrowserModule,
    HttpClientModule,
    MaterialModule,
    RouterModule.forRoot([
      { path: 'users', component: UserListComponent },
      { path: 'user/:id', component: UserDetailComponent },
      { path: 'tokens', component: TokenListComponent },
      { path: '**', component: UserListComponent }
    ])
  ],

  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
