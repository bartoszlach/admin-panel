import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError, fromEventPattern } from 'rxjs';
import { IUserToken } from '../model/user-token';
import { IToken } from '../model/token';

@Injectable({
  providedIn: 'root'
})
export class UserTokenService {
  private userTokenUrl = 'https://api.myjson.com/bins/umbue';

  constructor(private http: HttpClient) { }

  getTokens(): Observable<IToken[]> {
    return this.http.get<IToken[]>(this.userTokenUrl);
  }
}
