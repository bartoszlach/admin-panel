import { Injectable } from '@angular/core';
import { HttpClient, } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { IUser } from '../model/user';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private userUrl = 'https://api.myjson.com/bins/wrgru';
  //private userUrl = 'https://localhost:44380/api/users';
  constructor(private http: HttpClient) { }

  getUsers(): Observable<IUser[]> {
    return this.http.get<IUser[]>(this.userUrl);

  }

  getUser(id: number): Observable<IUser> {
    return this.getUsers()
    .pipe(map((users: IUser[]) => users.find(u => u.id === id)));
  }

}
