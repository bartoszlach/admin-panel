import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError, fromEventPattern } from 'rxjs';
import { catchError, tap, map } from 'rxjs/operators';
import { IToken } from '../model/token';

@Injectable({
  providedIn: 'root'
})
export class TokenService {
//private tokenUrl = 'https://localhost:44380/api/tokens';
private tokenUrl = 'https://api.myjson.com/bins/umbue';
  constructor(private http: HttpClient) { }

  // getTokens(): Observable<IToken[]> {
  //   return this.http.get<IToken[]>(this.tokenUrl);
  // }
  getTokens(): Observable<IToken[]> {
    return this.http.get<IToken[]>(this.tokenUrl)
    .pipe(tap(data => console.log('All: ' + JSON.stringify(data))), catchError(this.handleError));
  }
  private handleError(err: HttpErrorResponse){
    let errorMessage = ' ';
    if (err.error instanceof ErrorEvent) {
      errorMessage = 'Error: ' + err.error.message;
    } else {
      errorMessage = `Server code: ${err.status}, error message: ${err.message}`;
    }
    //console.error(errorMessage);
    return throwError(errorMessage);
  }

}
