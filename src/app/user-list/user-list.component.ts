import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { IUser } from '../model/user';
import { UserService } from '../service/user.service';
import {MatSort, MatTableDataSource } from '@angular/material';
@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit, AfterViewInit {
  errorMessage: any;
  @ViewChild(MatSort) sort: MatSort;
  constructor(private userService: UserService){}

  users: IUser[];
  displayedColumns: string[] = ['no', 'nick', 'points'];
  dataSource = new MatTableDataSource<IUser>();

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  ngOnInit() {
    this.userService.getUsers().subscribe(
      res => this.dataSource.data = res as IUser[],
      error => this.errorMessage = <any> error
    );
  }
  ngAfterViewInit(): void {
    this.dataSource.sort = this.sort;
  }

}
