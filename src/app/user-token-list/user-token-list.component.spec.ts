import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserTokenListComponent } from './user-token-list.component';

describe('UserTokenListComponent', () => {
  let component: UserTokenListComponent;
  let fixture: ComponentFixture<UserTokenListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserTokenListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserTokenListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
