import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { UserTokenService } from '../service/user-token.service';
import { IUserToken } from '../model/user-token';
import { IToken } from '../model/token';
import {MatSort, MatTableDataSource } from '@angular/material';


@Component({
  selector: 'app-user-token-list',
  templateUrl: './user-token-list.component.html',
  styleUrls: ['./user-token-list.component.css']
})
export class UserTokenListComponent implements OnInit, AfterViewInit {

  constructor(private userTokenService: UserTokenService) { }
  @ViewChild(MatSort) sort: MatSort;
  tokens: IToken[];

  displayedColumns: string[] = ['id', 'name', 'points', 'date'];
  dataSource = new MatTableDataSource<IToken>();

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  ngOnInit() {

    this.userTokenService.getTokens().subscribe(
      res => this.dataSource.data = res as IToken[]
    );
  }

  ngAfterViewInit(): void {
    this.dataSource.sort = this.sort;
  }

}
