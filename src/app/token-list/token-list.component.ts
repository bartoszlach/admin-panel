import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { IToken } from '../model/token';
import { TokenService } from '../service/token.service';
import {MatSort, MatTableDataSource } from '@angular/material';


@Component({
  selector: 'app-token-list',
  templateUrl: './token-list.component.html',
  styleUrls: ['./token-list.component.css']
})
export class TokenListComponent implements OnInit, AfterViewInit {
  @ViewChild(MatSort) sort: MatSort;
  constructor(private tokenService: TokenService) { }
  tokens: IToken[];

  displayedColumns: string[] = ['id', 'name', 'points', 'isMultiple', 'code'];
  dataSource = new MatTableDataSource<IToken>();

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  ngOnInit() {

    this.tokenService.getTokens().subscribe(
      res => this.dataSource.data = res as IToken[]
    );
  }

  ngAfterViewInit(): void {
    this.dataSource.sort = this.sort;
  }

}
